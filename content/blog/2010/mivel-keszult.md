---
Title: "Mivel készült?"
Date: 2010-02-26T19:00:00+02:00
Draft: false
Type: "post"
Summary: "Mivel készült?"
Slug: "MivelKészült"
Author: "Torma László"
---

Korábban úgy terveztem, hogy ha elkészül az FSF.hu Blog, akkor majd írok arról
egy blogbejegyzést, hogy hogyan is készült ez az oldal. Aztán rájöttem, hogy
ennél sokkal érdekesebb az, hogy mivel készült. Annál is inkább, mert maga az
oldal összerakása nem volt különösebben izgalmas, leszámítva az ilyenkor
szokásos köröket, mint hogy „Ha átírtam, miért nem változik? Ja, hogy a
backupot írtam át… remek…”, és hasonló dolgokat. Ezekkel azonban már mindenki
találkozott, aki valaha dolgozott számítógépen, szóval nem különösebben
érdekesek. Viszont az, hogy milyen alkalmazások használatával jött létre az
oldal, már annál érdekesebb. Már csak azért is, mert a blog.fsf.hu működése,
az Alapítvány nevéhez méltón, kizárólag szabad szoftverekkel valósul meg.
Érdemes tehát végigtekinteni ezeken a szoftvereken.

A blog.fsf.hu alatt egy szabad blogmotor, a [WordPress][1] fut: ez a [GPL
licenc][2] feltételeinek megfelelően szabadon elérhető bárki számára. Csak
lazán kapcsolódik ide, de érdemes megemlíteni, hogy a licenc [magyar
fordítása][3] is elérhető. A WordPress legfőbb előnye, hogy rendkívül
barátságos: könnyen, gyorsan lehet benne jól áttekinthető blogot készíteni, a
szerkesztőfelülete pedig szinte kényezteti a szerzőket, éppen csak a vállukat
nem nyomkodja gyengéden gépelés közben. Én magam egyébként nagy rajongója
vagyok a WordPressnek, és a saját oldalamnál is azt használom.

Az oldal egy [Ubuntu][4] 9.10 Desktopon készült el. Szintén csak lazán
kapcsolódik a témához, de a Magyar Ubuntu Közösség weboldala, az [ubuntu.hu][5]
számára az FSF.hu Alapítvány biztosítja a technikai hátteret. Az FSF.hu Blog
fejléce [GIMP][6] és [Inkscape][7] segítségével [készült][8]. A [GIMP][6] talán
az egyik legismertebb szabad szoftver, aminek története összefort a GNU/Linux
történetével: viszonylag kevesen tudják, hogy Tux, a Linux kabalapingvinje egy
korai (0.54-es) GIMP-pel készült, és Larry Ewing alkotása. Az [Inkscape][7] egy
kiváló vektorgrafikai alkalmazás, aminek használata szerencsére a hozzám
hasonló, grafikához nem különösebben értő laikusok számára sem okoz gondot.

Egy oldalt természetesen tesztelni kell, mielőtt kikerül élesben: én ezt úgy
oldottam meg, hogy egy virtuális gépre telepítettem egy Ubuntu 9.10-es
kiszolgálót, és először ott próbáltam ki mindent, mielőtt kikerült volna az
éles szerverre. Ehhez a KVM virtualizációs megoldást választottam, ugyanis ez
gyakorlatilag natív teljesítményt ad a virtuális gépen, és ezt a technológiát a
Linux kernel és az Ubuntu egyaránt hivatalosan támogatja. Hasonló megoldást
alkalmaz egyébként az FSF.hu Alapítvány szervere, a [Barack][9] is, ahol az
egyes szolgáltatások szeparálása ugyanezen virtualizációs technikával történik.
Maga a KVM használata egyébként rendkívül egyszerű, egyetlen jól paraméterezett
parancsot kell kiadni, és onnantól kezdve nagyon kényelmes a használata:
egyszerűen átirányítottam a megfelelő portokat a hostgép portjaira, így SSH
protokollon keresztül a [GNOME][10] asztali környezet saját fájlkezelőjéből, a
Nautilusból tudtam rá fájlokat másolgatni, és a böngészőmben néztem a készülő
oldalt. Ha pedig konzol kellett, akkor arra szintén ott volt az SSH.

Ha már szóba került a konzol, érdemes megemlíteni, hogy én a saját gépeimen
terminált mindig [Byobu][11]-val használok. A Byobu a [GNU Screen][12] terminál
multiplexerre épül, vagyis ez egy olyan eszköz, amivel egyetlen konzolt
sokszorozhatunk meg: hasonlóan például a Firefox böngészőhöz, itt is több
munkalapunk lehet, amik között egyszerűen tudunk lapozgatni. A Byobu a GNU
Screen használatát teszi barátságossá: F2-vel tudunk új lapot nyitni, a nyitott
lapok között az F3 és F4 billentyűk között lapozgathatunk. Alulra egyszerűen,
menüből beállíthatunk különféle kijelzőket, mint például az aktuális gép
IP-címe, vagy a bejelentkezett felhasználó neve. Sőt, ennek az alsó sávnak a
színét is ugyanilyen egyszerűen konfigurálhatjuk. Ez azért nagyon hasznos, mert
így minden gépen más-más színt beállítva könnyen megelőzhetjük, hogy véletlenül
rossz gépben adjunk ki egy parancsot. Természetesen mind a GNU Screen, mind
pedig a Byobu szabad szoftver.

Ahhoz, hogy bármilyen munka haladjon, a fentieken túl feltétlenül szükség van
még jó hangos zenére is. Mivel pedig a könnyűzene komoly dolog, nagyon nem
mindegy, hogy milyen kényelmes az a szoftver, amivel hallgatjuk. Szerencsére
GNU/Linuxon rengeteg jó zenelejátszó közül válogathatunk, én személy szerint
azonban az [Exaile][13]-t preferálom: ez egy [Pythonban][14] írt alkalmazás,
könnyen áttekinthető és kézre álló felhasználói felülettel. Jól illeszkedik az
általam használt rendszerbe, hiszen az üzeneteket az Ubuntu saját értesítési
rendszerén, a [NotifyOSD][15]-n keresztül jeleníti meg, és [GNOME Do][16] 
segítségével távvezérelhető. Amikor pedig végeztem az aznapra szánt munkával,
akkor levezetésképpen játszottam egy kicsit a [Mana Worlddel][17], ami egy
nyílt forráskódú MMORPG, vagyis sokrésztvevős, online szerepjáték.

Azt hiszem, ez a kis áttekintés jól mutatja, hogy milyen sokszínű a szabad
szoftverek világa. Persze amikor napi szinten használjuk ezeket a programokat,
akkor sokszor fel se tűnik: egyszerűen természetesnek vesszük, hogy szól a
zene, készül a weboldal, blogokat és mikroblogokat olvasgatunk, csevegünk az
ismerőseinkkel, vagyis használjuk a számítógépünket. Néha azonban érdemes egy
pillanatra megállni, és visszatekinteni: ezen alkalmazások ugyanis közösségi
erőfeszítéssel, a közösség számára jöttek létre. Bár ma már a szabad szoftverek
fejlesztésében jelentős szerepet töltenek be a vállalatok, de nem feledhetjük
el, hogy mindez közösségi kezdeményezésként indult, és mai napig 
kulcsfontosságú szerepet tölt be a fejlesztésben a közösség. A szabad szoftvert
mi alakítjuk, és értünk van. A szabadság minket szolgál.

[1]: https://wordpress.org
[2]: https://wordpress.org/about/license/
[3]: http://gnu.hu/gpl.html
[4]: https://ubuntu.com
[5]: https://ubuntu.hu
[6]: http://gimp.hu
[7]: http://inkscape.hu
[8]: https://isc.tamu.edu/~lewing/linux/notes.html
[9]: http://barack.fsf.hu
[10]: https://gnome.org
[11]: https://launchpad.net/byobu
[12]: https://www.gnu.org/software/screen/
[13]: https://www.exaile.org/
[14]: https://www.python.org/
[15]: https://wiki.ubuntu.com/NotifyOSD
[16]: http://do.davebsd.com/
[17]: https://www.themanaworld.org/

