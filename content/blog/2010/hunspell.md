---
Title: "Hunspell 1.2.9"
Date: 2010-03-04T20:00:00+02:00
Draft: false
Type: "post"
Summary: "Hunspell 1.2.9"
Slug: "Hunspell129"
Author: "Németh László"
---

Caolan MacNamara (Red Hat) teljes jogú adminisztrátorként csatlakozott a
Hunspell helyesírás-ellenőrző/morfológiai elemző fejlesztéséhez (eddig is az
egyik legaktívabb együttműködő volt a Hunspell javítások, foltok beküldésével).
Miután a CVS verziókezelő rendszer alá helyezte a kódot a múlt héten, egy tucat
foltot integrált. Ennek örömére még három fagyást, illetve kvázi lefagyást
okozó problémát javítottam. Az eredmény a Hunspell [1.2.9-es változata][1].

A CVS-ben közben frissült a kód, a Hunspell saját fejlesztői változatából
áttettem a MAP javaslattevő algoritmus bővítését. Ebben a MAP
ekvivalenciaosztályokba most már nem csak karaktereket, hanem
karaktersorozatokat is meg lehet adni egyszerű zárójelezéssel. A magyar
szótárban a MAP eddig csak az ékezetesítésre szolgált. Az affixumállományban
található MAP definíciót a következő sorral bővítve  a j és ly is egy
osztályba kerül:

{{< blockquote >}}MAP j(ly){{< /blockquote >}}

A régi helyesírási javaslatok a hűjéje szóra:

{{< blockquote >}}& hűjéje 9 0: hűjére, hűjébe, hájéje, hajéje,
héjéje, Hrabjéje, hűbérije, hűbére, hűdése{{< /blockquote >}}

Az újak:

{{< blockquote >}}& hűjéje 3 0: hülyéje, hűjére, hűjébe{{< /blockquote >}}

Ékezet nélküli szó javításánál még feltűnőbb a különbség:

Az előző változattal:

{{< blockquote >}}& hujeje 4 0: Huetje, hejehuja, Hummerje,
Höveje{{< /blockquote >}}

Most:

{{< blockquote >}}& hujeje 1 0: hülyéje{{< /blockquote >}}

A fejlesztés eredetileg a többszörös ékezetes betűket tartalmazó nyelvek, mint
a vietnami vagy a joruba számára készült, lehetőséget nyújtva a több UTF-8
karakterrel leírt betűk (pl. mint az ó alatt még pontot is tartalmazó joruba ọ́)
MAP osztályokba﻿ sorolására is (de a német ß/ss, vagy a ﬁ/fi ligatúrát
tartalmazó szavak kezelésére is jó).

[1]: https://sourceforge.net/projects/hunspell/files/Hunspell/1.2.9/hunspell-1.2.9.tar.gz/download
