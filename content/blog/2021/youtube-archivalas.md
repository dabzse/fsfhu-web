---
Title: "YouTube archiválás"
Date: 2021-03-11T21:16:00+01:00
Draft: true
Type: "post"
Summary: ""
Slug: "YouTubeArchiválás"
Author: "Meskó Balázs"
---

Felmerült az igény, hogy archiváljuk a YouTube csatornánkat, és egy PeerTube példányra költöztetjük a tartalmát. Mivel a videók eredetijét körülményes lett volna így évek múlva előkeríteni, így amellett döntöttünk, hogy letöltjük a YouTube-ra feltöltött videóinkat. Mivel relatív sok videóról van szó, így a `youtube-dl` használata mellett döntöttünk.

A `youtube-dl` nem csak egyetlen videót tud letölteni, hanem egy csatorna összes videóját, a videók leírásával együtt. Ez a letöltési sebességtől függően sokáig is eltarthat, de egy esetleges leállítás után is képes folytatni a letöltést. A teljes csatornánk letöltéséhez a következő parancsot használtuk:

```
youtube-dl -f best https://www.youtube.com/user/fsfhuszabadut --write-description
youtube-dl -f best https://www.youtube.com/channel/UCrMsde8yjoZyUvrFt7O0d6A --write-description
```

Az eredmény többszáz .mp4 illetve .description fájl, ez utóbbiak egyszerű szöveges fájlok az egyes videók leírásával. Az `-f best` kapcsoló azt állítja be, hogy a YouTube-on elérhető legjobb minőségű fájlokat töltse le, a `--write-description` pedig értelemszerűen a leírások letöltését kéri, enélkül csak a videófájlokat tölti le.
