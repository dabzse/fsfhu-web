---
Title: "GNOME 3.22"
Date: 2019-03-14T20:00:00+02:00
Draft: false
Type: "post"
Summary: "GNOME 3.32"
Slug: "GNOME322"
Author: "Meskó Balázs"
---

Megjelent a GNOME asztali környezet legújabb, 3.32-es verziója, amely az FSF.hu
Alapítvány aktivistáinak köszönhetően teljesen lefordított, magyar felülettel
érkezik. A kiadás részletei elolvashatóak a kiadási bejelentésben: 
https://help.gnome.org/misc/release-notes/3.32/
