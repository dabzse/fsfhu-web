---
Title: "Szabad Szoftver Konferencia és Kiállítás 2018 – előadói felhívás"
Date: 2018-02-13T21:09:33+02:00
Draft: false
Type: "post"
Summary: "Elindult az előadói jelentkezés a 2018-as Szabad Szoftver Konferenciára."
Slug: "SzSzK2018Jelentkezés"
Author: "Meskó Balázs"
---

Az FSF.hu Alapítvány 2018. május 12-én (szombaton) ismét megrendezi a Szabad Szoftver Konferencia és Kiállítást, melynek helyszíne az *ELTE Lágymányosi Campusa* lesz.  A konferenciára várjuk az előadók jelentkezését.

Amennyiben szeretnél előadással megjelenni a konferencián, kérem, legkésőbb 2018. március 2. (péntek) éjfélig jelentkezz a https://konf.fsf.hu/2018/eloadoknak weboldalon található űrlapon keresztül az előadásod rövid leírásának megadásával. Az idei konferencián külön Oktatás szekció is lesz (az elmaradt Linux az Oktatásban Konferencia helyett), így várjuk a szabad szoftverek/szabad tananyagok és oktatás témakörben is előadók jelentkezését.

A jelentkezők közül a szakmai zsűri 2018. március 11. éjfélig választja ki a végleges programba kerülő előadásokat. A döntésről minden jelentkezőt e-mailben is értesítünk.

