---
Title: "V. OpenScope nap"
Date: 2018-03-12T23:43:36+02:00
Draft: false
Type: "post"
Summary: "V. OpenScope nap"
Slug: "OpenScope5"
Author: "Kelemen Gábor"
---

Az <em>OpenScope esték</em> az FSF.hu alapítvány honosítással foglalkozó eseménysorozata. Az eseményre egyaránt várjuk a tapasztalt fordítókat, és a teljesen kezdőket is. Mindenki megtalálja a neki való feladatot, csatlakozzatok bátran!

Az ötödik este rendhagyó lesz: egész napos eseményként inkább nappal, mint este. 2018. március 15.-én, 10:00-tól 18:00-ig tartjuk online, a Telegram csoportunkban. Aktuális témánk az [Ubuntu Bionic][1], amely az Ubuntu soron következő hosszú távon támogatott verziója. A feladat az Ubuntu-specifikus modulok fordításainak frissítése lesz. Gyertek, és kapcsolódjatok be a munkánkba.

A részvételhez a következő helyen értek el minket:
- [Telegram csoport][2]
- [Facebook oldal][3]

Ott találkozunk!

[1]: https://translations.launchpad.net/ubuntu/bionic/+lang/hu?batch=300
[2]: https://t.me/joinchat/DIfJOgiG22VjI1f616aZWA
[3]: https://facebook.com/openscope.org
