---
Title: "II. OpenScope este"
Date: 2018-01-30T21:41:57+02:00
Draft: false
Type: "post"
Summary: "II. OpenScope este"
Slug: "OpenScope2"
Author: "Meskó Balázs"
---

Az <em>OpenScope esték</em> az FSF.hu alapítvány honosítással foglalkozó eseménysorozata. Az eseményre egyaránt várjuk a tapasztalt fordítókat, és a teljesen kezdőket is. Mindenki megtalálja a neki való feladatot, csatlakozzatok bátran!

A második este 2018. február 1.-jén lesz, 18:00-kor, a magyar Mozilla közösség jóvoltából a [D18 Irodaházban][1]. Témánk a [KDE][2] lesz, melynek aprója egy [friss HUP szavazás][3], mely szerint elég népszerű :) Sajnos a honosítási munkálatokkal jócskán le van maradva, ha más asztali környezetekhez viszonyítjuk.  Ha érdekel titeket a KDE honosítása, akkor gyertek, és kapcsolódjatok be a munkánkba. De ha csak maga a honosítási munka érdekel, vagy valamilyen más projekten szeretnél dolgozni, akkor is örömmel várunk.

A rendezvény pontos helyszíne: **1066 Budapest, Dessewffy utca 18-20.**

*Bejutás a helyszínen: gyertek be a kávézóba, és a pult mellett baloldalt elhaladva menjetek le a lépcsőn, majd az alagsor vége felé gyertek be a jobb oldalon lévő tárgyalóba. Ha esetleg később érkeznétek, és már zárva a kávézó, akkor hívjátok a +36-20/432-5328-as telefonszámot.*

Ha esetleg kérdésetek van, akkor a következő helyeken értek el minket:
- [Telegram csoport][4]
- [Facebook oldal][5]

Ott találkozunk!

Meskó Balázs,
szervező

[1]: http://d18.hu
[2]: https://www.kde.org/
[3]: https://hup.hu/szavazasok/20171215/hovd_2017_kedvenc_desktop_kornyezet
[4]: https://t.me/joinchat/DIfJOgiG22VjI1f616aZWA
[5]: https://facebook.com/openscope.org
