---
Title: "Ubuntu.hu költöztetés – szakembert keresünk"
Date: 2020-08-31T22:23:27+02:00
Draft: false
Type: "post"
Summary: "Szakembert keresünk az Ubuntu.hu költöztetésére"
Slug: "UbuntuHuKöltöztetés"
Author: "Meskó Balázs"
---

[Még májusban][1] állapítottuk meg, hogy szükséges az [ubuntu.hu][2] megújítása, és azt a megoldást választottuk, hogy a jelenlegi Drupal-alapú weboldal nagy részét statikus oldalgenerálóra cseréljük, a fórumot pedig átköltöztetjük egy saját [Discourse][3] példányra. A fórum Discourse-ra történő költöztetését szakember segítségével szeretnénk meglépni, így most felhívást teszünk, hogy erre *ajánlatokat várunk*. A költöztetés és az ajánlatkérés legfontosabb részleteiről ebből a PDF-fájlból tájékozódhattok:

{{< blogbutton label="Ajánlatkérés letöltése" href="assets/pdf/koltoztetes-ajanlatkeres.pdf" >}}

Az ajánlatokat **2020. szeptember 30-ig** várjuk az *ajanlat2020 (kukac) fsf (pont) hu* címre. Az esetleges felmerülő kérdéseket is elsősorban ide várjuk.

[1]: http://ubuntu.hu/node/45020
[2]: http://ubuntu.hu
[3]: https://www.discourse.org/
